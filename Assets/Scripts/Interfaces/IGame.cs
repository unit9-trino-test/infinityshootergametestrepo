using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IGame
{
    void Started();
    void Ended();
}
