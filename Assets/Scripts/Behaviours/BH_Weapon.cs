using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BH_Weapon : MonoBehaviour
{
    [SerializeField]
    public ParticleSystem spark;
    [SerializeField]
    MeshRenderer meshRender;
    byte color;
    byte currenColor = 1;
    Coroutine changing = null;


    public void SetEffect(BaseAttributes attributes)
    {
        color = 0;
        if (attributes.health > 0)
        {
            color |= 1;
        }
        if (attributes.defense > 0) {
            color |= 2;
        }
        if (attributes.dextery > 0)
        {
            color |= 4;
        }
        if (attributes.attack > 0)
        {
            color |= 8;
        }
        if (color!=0 && changing == null)
        {
            StartCoroutine(ChangeColor());
        }
    }
    IEnumerator ChangeColor()
    {
        while (color != 0)
        {
            switch (currenColor&color)
            {
                case 1:
                    meshRender.material.color = Color.green;
                    break;
                case 2:
                    meshRender.material.color = Color.blue;
                    break;
                case 4:
                    meshRender.material.color = Color.yellow;
                    break;
                case 8:
                    meshRender.material.color = Color.red;
                    break;
            }
            currenColor <<= 1;
            if (currenColor == 0) currenColor = 1;
            yield return new WaitForSeconds(1);
        }
        changing = null;
    }
    public void Fireup()
    {
        spark.Emit(Random.Range(1,5));
    }
}
