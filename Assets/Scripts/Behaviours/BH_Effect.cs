using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BH_Effect : BH_Character
{
    Vector3 target;
    private void Update()
    {
        transform.position = Vector3.Lerp(transform.position, target, Time.deltaTime);
    }
    public override void Ended()
    {
        Destroy(gameObject);
    }

    public override void TakeDamage(float amount)
    {
        base.TakeDamage(amount);
        if (attributes.health <= 0)
        {
            model.manager.instance.player.ApplyEffect(attributes);
            Destroy(gameObject);
        }
    }
    private void OnTriggerEnter(Collider other)
    {
        Debug.Log($"{name} collided with {other.gameObject.name}");
        Destroy(gameObject);
    }
    public override void Started()
    {
        base.Started();
        target = model.manager.instance.player.transform.position;
    }
}
