using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BH_Player : BH_Actor
{
    [SerializeField]
    GameObject impactPrefab;
    [SerializeField]
    Transform aim;
    [SerializeField]
    BH_Weapon weapon;
    public void ApplyEffect(BaseAttributes attributes)
    {
        
        attributes.Apply(attributes);
        StartCoroutine(CoolDown(attributes.cooldown));
    }
    public void RestoreEffects()
    {
        attributes.Restore(model.attributes);
    }
    IEnumerator CoolDown(float cooldown)
    {
        yield return new WaitForSeconds(cooldown);
        RestoreEffects();
    }
    public void Fireup()
    {
        RaycastHit hit;
        if (Physics.Raycast(transform.position, transform.forward, out hit))
        {
            Collider collider = hit.collider;

            BH_Character character = collider.GetComponent<BH_Character>();
            if (character)
            {
                if (collider.CompareTag("Enemy"))
                {
                    Transform impact = Instantiate(impactPrefab, hit.point, Quaternion.identity).transform;
                    impact.forward = hit.normal;
                    character.TakeDamage(MakeDamage());
                }
                else if (collider.CompareTag("Powerup"))
                {
                    ApplyEffect(character.attributes);
                }
            }
        }
    }
    public override void TakeDamage(float amount)
    {
        base.TakeDamage(amount);
        if (attributes.health <= 0)
        {
            model.manager.instance.Ended();
        }
    }
    public override void Ended()
    {

    }

    public void OnPointerEnter()
    {

    }
    public void OnPointerExit()
    {

    }
    public void OnPointerClick()
    {

    }
}
