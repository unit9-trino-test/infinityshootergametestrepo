using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class BH_Enemy : BH_Actor
{
    [SerializeField]
    NavMeshAgent agent;
    float cooldown = 0;
    BH_Character player;
    
    void Update()
    {
        if (player)
        {
            cooldown += Time.deltaTime;
            if (cooldown > Random.Range(0f, attributes.cooldown))
            {
                cooldown = 0;
                if (Random.Range(0, 3) > 1/attributes.dextery)
                    player.TakeDamage(MakeDamage());
            }
        }
    }
    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            agent.isStopped = true;
            player = other.GetComponent<BH_Character>();
            animator.SetBool("Walking", false);
        }
    }
    private void OnTriggerExit(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            player = null;
            animator.SetBool("Walking", true);
            SeekPlayer();
            agent.isStopped = false;
        }
    }
    private void SeekPlayer()
    {
        agent.SetDestination(model.manager.instance.player.transform.position);
    }
    public override void TakeDamage(float amount)
    {
        base.TakeDamage(amount);
        
        if (attributes.health <= 0)
        {
            animator.SetTrigger("Die");
            model.manager.instance.AddPoints(((SC_Enemy)model).points);
        }
        else
        {
            agent.isStopped = true;
            animator.SetTrigger("Hit");
        }
    }
    public override void Started()
    {
        base.Started();
        SeekPlayer();
        animator.SetBool("Walking", true);
    }
    public override void Ended()
    {
        animator.SetTrigger("Die");
    }
    public void FinishDie()
    {
        Destroy(gameObject);
    }
    public void FinishHit()
    {
        agent.isStopped = false;
    }
}
