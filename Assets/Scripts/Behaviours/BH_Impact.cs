using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BH_Impact : MonoBehaviour
{
    void Start()
    {
        StartCoroutine(Die());
    }
    IEnumerator Die()
    {
        yield return new WaitForSeconds(0.3f);
        Destroy(gameObject);
    }

}
