using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class BH_Character : MonoBehaviour, IDamage, IGame
{
    public BaseAttributes attributes;
    [SerializeField]
    protected SC_Character model;
    private void Start()
    {
        Started();
    }

    public virtual void TakeDamage(float amount)
    {
        amount = amount - amount * attributes.defense * 0.1f;
        Debug.Log($"<color=red>[{name}] Taking damage {amount}</color>");
        attributes.health -= amount;
    }

    public virtual void Started()
    {
        attributes = new BaseAttributes();
        attributes.Restore(model.attributes);
    }
    public abstract void Ended();

    public virtual float MakeDamage()
    {
        return model.attributes.attack * attributes.attack;
    }
}
