using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class BH_Actor : BH_Character
{

    [SerializeField]
    protected Animator animator;

    public override float MakeDamage()
    {
        animator.SetTrigger("Attack");
        return base.MakeDamage();
    }
}
