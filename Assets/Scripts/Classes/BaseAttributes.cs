using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class BaseAttributes
{
    public float health;
    public float attack;
    public float defense;
    public float dextery;
    public float cooldown;

    public void Apply(BaseAttributes attributes)
    {
        this.health += attributes.health;
        this.attack += attributes.attack;
        this.defense += attributes.defense;
        this.dextery += attributes.dextery;
    }
    public void Restore(BaseAttributes attributes)
    {
        this.health = attributes.health;
        this.attack = attributes.attack;
        this.defense = attributes.defense;
        this.dextery = attributes.dextery;
    }
}
