using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public class GameManager : MonoBehaviour, IGame
{
    public BH_Player player;
    [SerializeField]
    GameObject[] startObjects;
    [SerializeField]
    GameObject[] endObjects;
    [SerializeField]
    Text pointsText;
    [SerializeField]
    Transform[] spawnsPoints;
    public SC_GameManager manager;
    float timer = 0;
    float respawn = 0;
    int totalPoints = 0;
    int enemiesCount=0;
    int powerupCount=0;
    UnityAction started;
    UnityAction ended;
    private void Start()
    {
        manager.state = GameState.StandBy;
        manager.SetManager(this);
    }
    private void OnDestroy()
    {
        manager.Close();
    }

    private void Update()
    {
        if (manager.state != GameState.Started) return;
        respawn += Time.deltaTime;
        if (respawn > manager.respawn)
        {
            respawn = 0;
            if (enemiesCount < manager.maxEnemies)
            {
                if (powerupCount < manager.maxPowerUps)
                {
                    if (Random.Range(0,3) < 2)
                    {
                        manager.Spawn(manager.GetNextEnemyPrefab(), spawnsPoints[Random.Range(0, spawnsPoints.Length)]);
                    }
                    else
                    {
                        manager.Spawn(manager.GetNextEffectPrefab(), spawnsPoints[Random.Range(0, spawnsPoints.Length)]);
                    }
                }
                else
                {
                    manager.Spawn(manager.GetNextEnemyPrefab(), spawnsPoints[Random.Range(0, spawnsPoints.Length)]);
                }
            }
            else if (powerupCount < manager.maxPowerUps)
            {
                manager.Spawn(manager.GetNextEffectPrefab(), spawnsPoints[Random.Range(0, spawnsPoints.Length)]);
            }
        }
        timer += Time.deltaTime;
        if (timer > manager.time)
        {
            Ended();
        }
    }
    public void AddPoints(int points)
    {
        totalPoints += points;
        pointsText.text = totalPoints.ToString();

    }
    public void Started()
    {
        Debug.Log("Starting game...");
        manager.state = GameState.Started;
        started?.Invoke();
        EnableObjects(true);
    }
    public void Ended()
    {
        manager.state = GameState.Ended;
        ended?.Invoke();
        EnableObjects(false);
    }
    private void EnableObjects(bool enable)
    {
        for (int i=0; i < startObjects.Length; i++)
        {
            startObjects[i].SetActive(enable);
        }
        for (int i = 0; i < endObjects.Length; i++)
        {
            endObjects[i].SetActive(!enable);
        }
    }

}
public enum GameState
{
    StandBy,
    Started,
    Paused,
    Ended
}