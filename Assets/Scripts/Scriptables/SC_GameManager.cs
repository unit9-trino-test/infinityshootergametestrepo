using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "GameManager", menuName = "GameManager")]
public class SC_GameManager : ScriptableObject
{
    public GameObject[] effects;
    public GameObject[] enemies;
    public int maxEnemies;
    public int maxPowerUps;
    public int time;
    public float respawn;
    public GameState state;
    public GameManager instance;

    public void SetManager(GameManager manager)
    {
        this.instance = manager;
    }
    public void Close()
    {
        this.instance = null;
    }

    public void Spawn(GameObject prefab, Transform point)
    {
        GameObject.Instantiate(prefab, point.position, point.rotation).GetComponent<BH_Character>();
    }
    public GameObject GetNextEnemyPrefab()
    {
        return enemies[Random.Range(0, enemies.Length)];
    }
    public GameObject GetNextEffectPrefab()
    {
        return effects[Random.Range(0, enemies.Length)];
    }
}
