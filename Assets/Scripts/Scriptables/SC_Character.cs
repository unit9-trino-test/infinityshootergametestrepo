using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SC_Character : ScriptableObject
{
    [SerializeField]
    public SC_GameManager manager;
    public BaseAttributes attributes;
}
